import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  ngOnInit(){

  }
  show:boolean=false
  title = 'my-app';
  add(){
    this.show=!this.show
    localStorage.setItem("title","the vedio is uploaded")
  }
  remove(){
    this.show=!this.show
    localStorage.removeItem("title")
  }
}
